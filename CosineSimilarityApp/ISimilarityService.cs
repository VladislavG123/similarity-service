using System.Threading.Tasks;

namespace CosineSimilarityApp
{
    public interface ISimilarityService
    {
        double GetSimilarity(string line1, string line2);
    }
}