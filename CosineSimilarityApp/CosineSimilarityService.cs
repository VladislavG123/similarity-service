using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CosineSimilarityApp
{
    public class CosineSimilarityService : ISimilarityService
    {
        public double GetSimilarity(string line1, string line2)
        {
            // Dividing both lines to unique words
            var uniquePunctuation = (line1 + " " + line2).Where(Char.IsPunctuation).Distinct().ToArray();
            var uniqueWords = (line1 + " " + line2).Split().Select(x => x.Trim(uniquePunctuation)).Distinct().ToList();
            
            // Creation of vectors of unique words counting
            List<int> vector1 = new();
            var punctuationLine1 = line1.Where(Char.IsPunctuation).Distinct().ToArray();
            var wordsLine1 = line1.Split().Select(x => x.Trim(punctuationLine1)).ToList();
            
            List<int> vector2 = new();
            var punctuationLine2 = line2.Where(Char.IsPunctuation).Distinct().ToArray();
            var wordsLine2 = line2.Split().Select(x => x.Trim(punctuationLine2)).ToList();

            foreach (var uniqueWord in uniqueWords)
            {
                vector1.Add(wordsLine1.Count(x => x.Equals(uniqueWord)));
                
                vector2.Add(wordsLine2.Count(x => x.Equals(uniqueWord)));
            }
            
            // Finding of multiplication of vectors
            var numerator = 1;
            for (int i = 0; i < uniqueWords.Count; i++)
            {
                numerator += vector1[i] * vector2[i];
            }
            
            // Finding of modules of vectors
            var vector1Module = Math.Sqrt(vector1.Aggregate(0, (current, item) => current + item * item));
            var vector2Module = Math.Sqrt(vector2.Aggregate(0, (current, item) => current + item * item));

            var similarity = numerator / (vector1Module * vector2Module) * 100;

            return similarity;
        }
    }
}